package io.nuvel.engine.math;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class Vector2Test
{
	@Test
	public void IntConstructorTest()
	{
		int x = 123;
		int y = 456;

		Vector2 v = new Vector2(x, y);

		assertTrue(v.x == x && v.y == y);
	}

	@Test
	public void VectorConstructorTest()
	{
		Vector2 t = new Vector2(123, 456);
		Vector2 v = new Vector2(t);

		assertTrue(v.x == t.x && v.y == t.y);
	}
}