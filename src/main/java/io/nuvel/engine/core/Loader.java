package io.nuvel.engine.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;

import io.nuvel.engine.data.Flow;
import io.nuvel.engine.data.Character;

/**
 * Class to handle loading of assets and
 * deserialization of the .lt files
 * <br>
 * P.S. Loader is a singleton!
 */
public class Loader
{
	/// Singleton instance
	private static Loader instance;

	/// Public variables
	public Flow flow;

	/// Private variables
	private Ini settings;
	private Ini menu;
	private List<Character> characters = new ArrayList<Character>();

	/**
	 * Constructor that loads and initializes
	 * all the data for the game
	 */
	private Loader()
	{
		//#region Load the settings.ini file
		try
		{
			InputStream is = Loader.class.getClassLoader().getResourceAsStream("settings.ini");
			settings = new Ini(is);
		}
		catch (InvalidFileFormatException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		//#endregion


		//#region Load the menu.ini file
		try
		{
			InputStream is = Loader.class.getClassLoader().getResourceAsStream("menu.ini");
			menu = new Ini(is);
		}
		catch (InvalidFileFormatException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		//#endregion


		//#region Load the flow.json file
		flow = new Flow(Loader.class.getClassLoader().getResourceAsStream("flow.json"));
		//#endregion


		//#region Load the characters
		/// Get a resource stream of the characters directory
		InputStream is = Loader.class.getClassLoader().getResourceAsStream("characters/index.fs");
		// Create a buffered reader so i can go through the strings
		// of the files in the directory
		BufferedReader br = new BufferedReader(new InputStreamReader(is));

		// Loop through all the files in the character directory
		br.lines().forEach(s -> {
			/// Load the character file data
			try
			{
				InputStream isChar = Loader.class.getClassLoader().getResourceAsStream("characters/" + s);
				Ini iniChar = new Ini(isChar);
				int id = Integer.parseInt(s.replace(".character", ""));
				int imageId = Integer.parseInt(iniChar.get("Info", "Image"));

				characters.add(new Character(id, iniChar.get("Info", "Name"), imageId));
			}
			catch (InvalidFileFormatException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		});
		//#endregion
	}

	/**
	 * Returns the instance of the class, or
	 * instantiates and returns a new Loader
	 * 
	 * @return the instance of Loader
	 */
	public static Loader Instance()
	{
        if (instance == null)
		{
            instance = new Loader();
        }
        
        return instance;
    }
	

	/// Getters
	/**
	 * Returns the parsed settings file in
	 * format GetSettings().get(header, key)
	 * 
	 * @return a parsed settings.ini file
	 */
	public Ini GetSettings()
	{
		return settings;
	}

	public Character GetCharacterFromIndex(int index)
	{
		for (Character character : characters)
		{
			if (character.id == index)
			{
				return character;
			}
		}

		return null;
	}

	/**
	 * Returns the parsed menu file in
	 * format GetMenu().get(header, key)
	 * 
	 * @return a parsed menu.ini file
	 */
	public Ini GetMenu()
	{
		return menu;
	}

	/**
	 * Returns a parsed list of all the
	 * characters and their data
	 * 
	 * @return a list of parsed .character files
	 */
	public List<Character> GetCharacters()
	{
		return characters;
	}
}