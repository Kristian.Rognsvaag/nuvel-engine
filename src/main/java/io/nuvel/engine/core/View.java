package io.nuvel.engine.core;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JComponent;

import io.nuvel.engine.data.Line;
import io.nuvel.engine.data.Scene;
import io.nuvel.engine.math.Vector2;
import io.nuvel.engine.data.Character;

public class View extends JComponent
{
	private Loader loader;

	private Graphics graphics;
	private int width;
	private int height;

	private boolean showSplash = false;
	private Splash splash;

	public View()
	{
		/// Cache the loader
		this.loader = Loader.Instance();

		this.setFocusable(true);

		/// Draw splash screen
		// Turn on drawing of splash
		showSplash = true;

		// Create splash screen instance
		// This will also have a callback to when
		// the duration is over and the splash screen
		// shouldn't be shown anymore.
		this.splash = new Splash(new Vector2(400, 400), "assets/images/nuvelSplash.png", "Nuvel Engine", new Color(50, 146, 232), 4, new Callable<Void>()
		{
			@Override
			public Void call() throws Exception
			{
				// Turn off drawing of splash
				showSplash = false;
				// Repaint the game
				repaint();

				// Must return null as it is a Void function
				return null;
			}
		});
	}

	@Override
	public void paint(Graphics g)
	{
		/// Run super
		super.paint(g);

		/// Initialize variables
		// Store the graphics object
		// It's better to globally store the graphics
		// object so one doesn't have one big recursively
		// nested chain of references to the graphics.
		graphics = g;
		// Cache the size of window
		width = getWidth();
		height = getHeight();

		/// Turn on anti-aliasing
		// This tries to smooth out the jagged
		// edges that may occur when you scale images
		Graphics2D g2d = (Graphics2D) graphics;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		graphics = g2d;


		/// Draw splash screen
		if (showSplash)
		{
			splash.Show(graphics, this, new Vector2(width, height));
		}
		/// Draw game loop
		else
		{
			GameLoop();
		}
	}

	private void GameLoop()
	{
		Scene cs = loader.flow.GetCurrentScene();

		/// Draw background
		String backgroundPath = "assets/backgrounds/" + cs.background + ".png";
		graphics.drawImage(LoaderHelper.LoadImageFromPath(backgroundPath), 0, 0, width, height, this);


		/// Draw characters
		List<Character> characters = new ArrayList<Character>();
		for (int characterIndex : cs.characters)
		{
			characters.add(loader.GetCharacterFromIndex(characterIndex));
		}
		for (int i = 0; i < characters.size(); i++)
		{
			Image characterImg = LoaderHelper.LoadImageFromPath("assets/characters/" + characters.get(i).image + ".png");
			characterImg = characterImg.getScaledInstance(width, height / 2, Image.SCALE_DEFAULT);
			graphics.drawImage(characterImg, 0, height - (height / 2), this);
		}


		/// Draw text box
		int textboxPadding = 10;
		Image textboxImg = LoaderHelper.LoadImageFromPath("assets/images/textbox.png");
		graphics.drawImage(textboxImg, (width / 2) - (textboxImg.getWidth(null) / 2), height - textboxImg.getHeight(null) - textboxPadding, this);


		/// Draw name box and name
		Line line = cs.GetLine();
		Character character = loader.GetCharacterFromIndex(line.character);

		if (character != null)
		{
			// Draw name box
			int nameboxLeftPadding = 10;
			Image nameboxImg = LoaderHelper.LoadImageFromPath("assets/images/namebox.png");
			graphics.drawImage(nameboxImg, (width / 2) - (textboxImg.getWidth(null) / 2) + nameboxLeftPadding,
							height - textboxImg.getHeight(null) - textboxPadding - nameboxImg.getHeight(null), this);


			// Draw name
			int fontSize = 20;
			Font font = new Font("SansSerif", Font.BOLD, fontSize);
			
			graphics.setColor(Color.BLACK);
			graphics.setFont(font);
			graphics.drawString(character.name, (width / 2) - (textboxImg.getWidth(null) / 2) + nameboxLeftPadding - (graphics.getFontMetrics(font).stringWidth(character.name) / 2) + (nameboxImg.getWidth(null) / 2),
								height - textboxImg.getHeight(null) - (graphics.getFontMetrics(font).getHeight() / (fontSize / 4)) - (nameboxImg.getHeight(null) / 2));
		}


		/// Draw lines
		int fontSize = 16;
		int textPadding = 80;
		int textWidthPadding = 120;

		graphics.setColor(Color.WHITE);
		Font font = new Font("SansSerif", Font.BOLD, fontSize);
		graphics.setFont(font);

		List<String> lines = new ArrayList<String>();
		if (graphics.getFontMetrics(font).stringWidth(cs.GetLine().text) > textboxImg.getWidth(null) - textWidthPadding)
		{
			int chars = (int) Math.ceil((textboxImg.getWidth(this) - textWidthPadding) / Math.floor(graphics.getFontMetrics(font).stringWidth(cs.GetLine().text) / cs.GetLine().text.length()));

			String rawLine = cs.GetLine().text;
			for (int i = 0; i < 8; i++)
			{
				String[] split = rawLine.substring(0, chars).split(" ");
				String splt = String.join(" ", Arrays.copyOf(split, split.length - 1));
				String rest = rawLine.substring(splt.length() + 1);

				lines.add(splt);

				if (graphics.getFontMetrics(font).stringWidth(rest) > textboxImg.getWidth(null) - textWidthPadding)
				{
					rawLine = rest;
				}
				else
				{
					lines.add(rest);
					break;
				}
			}
		}
		else
		{
			lines.add(cs.GetLine().text);
		}


		// Draw lines
		int textHeightPadding = 0;
		int yOffset = (int) Math.ceil((lines.size() - 1) * (graphics.getFontMetrics(font).getHeight() / 2));
		for (int i = 0; i < lines.size(); i++)
		{
			graphics.drawString(lines.get(i), (width / 2) - ((lines.get(i).length() * fontSize) / 4), height - textPadding + textHeightPadding - yOffset);
			textHeightPadding += graphics.getFontMetrics(font).getHeight();
		}
	}
}