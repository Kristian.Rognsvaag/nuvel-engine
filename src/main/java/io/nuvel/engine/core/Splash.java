package io.nuvel.engine.core;

import java.awt.Color;
import java.awt.Image;
import java.awt.Graphics;
import java.awt.image.ImageObserver;
import java.util.concurrent.Callable;
import java.awt.Font;

import io.nuvel.engine.math.Vector2;

public class Splash
{
	private Vector2 size;
	private Image logo;
	private String text;
	private Color backgroundColor;

	/**
	 * Constructor to make a new splash screen object
	 * 
	 * @param size of logo
	 * @param logo path to asset
	 * @param text to be shown
	 * @param backgroundColor
	 * @param duration of splash screen in seconds
	 * @param func callback to when duration is over
	 */
	public Splash(Vector2 size, String logo, String text, Color backgroundColor, int duration, Callable<Void> func)
	{
		this.size = size;
		this.text = text;
		this.backgroundColor = backgroundColor;

		this.logo = LoaderHelper.LoadImageFromPath(logo);
		
		// Create a seperate thread to handle splash screen duration
		Thread thread = new Thread()
		{
			public void run()
			{
				// Suspend the thread
				try
				{
					Thread.sleep(duration * 1000);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}

				// Call the callback function so the view can
				// disable the show splash handling
				try
				{
					func.call();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		};
		// Run the thread
		thread.start();
	}

	/**
	 * Show the splash screen
	 * 
	 * @param graphics the graphics object to render to
	 * @param observer the image observer
	 * @param window the size of the game window
	 */
	public void Show(Graphics graphics, ImageObserver observer, Vector2 window)
	{
		/// Draw logo
		graphics.setColor(backgroundColor);
		graphics.fillRect(0, 0, window.x, window.y);
		graphics.drawImage(logo, (window.x / 2) - (size.x / 2), (window.y / 2) - (size.y / 2), size.x, size.y, observer);

		/// Draw name
		int textPadding = 80;
		Font font = new Font("SansSerif", Font.BOLD, 50);
		
		graphics.setFont(font);
		graphics.setColor(Color.WHITE);
		graphics.drawString(text, (window.x / 2) - (graphics.getFontMetrics(font).stringWidth(text) / 2), (window.y / 2) + (size.y / 2) + textPadding);
	}
}