package io.nuvel.engine.core;

import java.awt.Image;
import java.io.IOException;

import javax.imageio.ImageIO;

public class LoaderHelper
{
	/**
	 * Loads in an image from a path, and
	 * converts it to the java.awt.Image object
	 * 
	 * @param path path to the image asset
	 * @return image awt object from the path or null
	 */
	public static Image LoadImageFromPath(String path)
	{
		try
		{
			return ImageIO.read(LoaderHelper.class.getClassLoader().getResourceAsStream(path));
		}
		catch (IOException e)
		{
			e.printStackTrace();

			return null;
		}
	}
}