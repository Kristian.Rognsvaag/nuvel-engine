package io.nuvel.engine.core;

import javax.swing.JFrame;
import java.awt.Color;
import java.awt.GraphicsDevice;
import java.awt.Image;
import java.awt.GraphicsEnvironment;

import io.nuvel.engine.math.Vector2;

/**
 * Class to create and initialize a Swing JFrame window
 */
public class Window extends JFrame
{
	/**
	 * Constructor that sets the title, size and view
	 * 
	 * @param title of the window
	 * @param size of the window
	 * @param view the JComponent that draws to the window
	 */
	public Window(String title, Vector2 size, View view)
	{
		this.setTitle(title);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setContentPane(view);

		/// Set fullscreen
		// Get the graphical context device
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		// Check if fullscreen is supported
		// if (gd.isFullScreenSupported())
		if (false)
		{
			gd.setFullScreenWindow(this);
		}
		else
		{
			this.setSize(size.x, size.y);
			gd.setFullScreenWindow(null);
		}
		
		this.setVisible(true);
	}

	/**
	 * Constructor that sets the title, size, background color and view
	 * 
	 * @param title of the window
	 * @param size of the window
	 * @param view the JComponent that draws to the window
	 * @param background color of the window
	 */
	public Window(String title, Vector2 size, View view, Color background)
	{
		this(title, size, view);

		this.getContentPane().setBackground(background);
	}

	/**
	 * Set the window icon with a path to the image
	 * 
	 * @param imagePath path to the image asset
	 */
	public void setWindowIcon(String imagePath)
	{
		this.setIconImage(LoaderHelper.LoadImageFromPath(imagePath));
	}

	/**
	 * Set the window icon with an image AWT object
	 * 
	 * @param image awt object to use
	 */
	public void setWindowIcon(Image image)
	{
		this.setIconImage(image);
	}
}