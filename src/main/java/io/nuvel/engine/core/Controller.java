package io.nuvel.engine.core;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import javax.swing.event.MouseInputListener;

public class Controller implements java.awt.event.KeyListener, MouseInputListener
{
	private Loader loader;
	private View view;

	public Controller(View view)
	{
		this.loader = Loader.Instance();
		this.view = view;

		// Adds this key and event listener to the window loop
		this.view.addKeyListener(this);
		this.view.addMouseListener(this);
	}


	/// Key listeners
	@Override
	public void keyTyped(KeyEvent e)
	{
		
	}

	@Override
	public void keyPressed(KeyEvent e)
	{
		System.out.println(e);

		if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
		{
			System.exit(0);
		}
	}

	@Override
	public void keyReleased(KeyEvent e)
	{
		
	}


	/// Mouse listeners
	@Override
	public void mouseClicked(MouseEvent e)
	{
		
	}

	@Override
	public void mousePressed(MouseEvent e)
	{
		if (loader.flow.HasNextLine())
		{
			loader.flow.NextLine();
		}
		else
		{
			if (!loader.flow.NextScene())
			{
				// TODO: Go to menu instead of exiting
				System.exit(0);
			}
		}

		view.repaint();
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
		
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
		
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
		
	}

	@Override
	public void mouseDragged(MouseEvent e)
	{

	}

	@Override
	public void mouseMoved(MouseEvent e)
	{
		
	}
}