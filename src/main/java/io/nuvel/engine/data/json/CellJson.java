package io.nuvel.engine.data.json;

import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class to aid in deserialising the
 * flow.json string safely
 */
public class CellJson
{
	@JsonProperty("type")
	public String type;

	@JsonProperty("position")
	public Position position;

	@JsonProperty("size")
	public Size size;

	@JsonProperty("angle")
	public String angle;

	@JsonProperty("id")
	public UUID id;

	@JsonProperty("z")
	public int z;

	@JsonProperty("attrs")
	public Map<String, Map<String, Object>> attrs;

	@JsonProperty("source")
	public Map<String, UUID> source;

	@JsonProperty("target")
	public Map<String, UUID> target;

	@JsonProperty("router")
	public Map<String, String> router;

	@JsonProperty("connector")
	public Map<String, String> connector;
}

class Position
{
	@JsonProperty("x")
	public int x;

	@JsonProperty("y")
	public int y;
}

class Size
{
	@JsonProperty("width")
	public int width;

	@JsonProperty("height")
	public int height;
}