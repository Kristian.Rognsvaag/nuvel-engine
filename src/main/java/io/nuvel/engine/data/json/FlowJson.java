package io.nuvel.engine.data.json;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class to be used purely for
 * deserializing a json string
 * to java safely
 */
public class FlowJson
{
	@JsonProperty("cells")
	public CellJson[] cells;
}