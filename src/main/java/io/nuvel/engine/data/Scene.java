package io.nuvel.engine.data;

import java.util.List;
import java.util.UUID;

/**
 * Class to hold data for a scene
 */
public class Scene
{
	public UUID id;
	public boolean start;
	public UUID target; // If target is null, then this is the last scene

	public int background;
	public int backgroundMusic;
	public List<Line> lines;
	public int[] characters;

	public int currentLine = 0;

	/**
	 * Constructor with id, start and target
	 * 
	 * @param id
	 * @param start
	 * @param target
	 */
	public Scene(UUID id, boolean start, UUID target)
	{
		this.id = id;
		this.start = start;
		this.target = target;
	}

	public void NextLine()
	{
		currentLine++;
	}

	/// Getters
	public Line GetLine()
	{
		return lines.get(currentLine);
	}

	/// Setters
	/**
	 * Sets the background id
	 * 
	 * @param background id to set
	 */
	public void SetBackground(int background)
	{
		this.background = background;
	}

	/**
	 * Sets the background music id
	 * 
	 * @param backgroundMusic id to set
	 */
	public void SetBackgroundMusic(int backgroundMusic)
	{
		this.backgroundMusic = backgroundMusic;
	}

	/**
	 * Sets the lines
	 * 
	 * @param lines to set
	 */
	public void SetLines(List<Line> lines)
	{
		this.lines = lines;
	}

	public void SetCharacters(int[] characters)
	{
		this.characters = characters;
	}

	@Override
	public String toString()
	{
		return String.format("{\n\tid: %s, start: %s, target: %s, bg: %d, bgMusic: %d\n\tlines: %s\n}", id, start, target, background, backgroundMusic, lines);
	}
}