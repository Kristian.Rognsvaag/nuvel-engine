package io.nuvel.engine.data;

/**
 * Class to hold data for each line a character says
 */
public class Line
{
	public int character;
	public String text;

	/**
	 * Constructor for character and text
	 * 
	 * @param character the is talking
	 * @param text to be displayed
	 */
	public Line(int character, String text)
	{
		this.character = character;
		this.text = text;
	}

	@Override
	public String toString() {
		return String.format("{ character: %d, text: %s }", character, text);
	}
}