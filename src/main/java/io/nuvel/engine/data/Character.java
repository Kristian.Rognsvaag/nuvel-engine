package io.nuvel.engine.data;

/**
 * Class to store data of a character
 */
public class Character
{
	public int id;
	public String name;
	public int image;

	/**
	 * Constructor with id, name and image id
	 * 
	 * @param id of the character
	 * @param name of the character
	 * @param image id of the character
	 */
	public Character(int id, String name, int image)
	{
		this.id = id;
		this.name = name;
		this.image = image;
	}

	@Override
	public String toString()
	{
		return String.format("{ id: %d, name: %s, image: %d }", id, name, image);
	}
}