package io.nuvel.engine.data;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.nuvel.engine.data.json.CellJson;
import io.nuvel.engine.data.json.FlowJson;

/**
 * Class to have control over the entire flow
 * of the game and the scenes
 */
public class Flow
{
	public UUID currentScene;

	private List<Scene> scenes = new ArrayList<Scene>();

	/**
	 * Constructor with json
	 * 
	 * @param json InputStream from resources
	 */
	public Flow(InputStream json)
	{
		/// Load the raw flow.json file and parse the data
		// Then clean up and extract the wanted data into
		// the final list of scenes with their id and references
		try
		{
			// The reason I've created a seperate class for deserialization of
			// json to java, is because this is a safe/stable way instead of
			// insecurely converting the json into one big Map<String, Object>.
			FlowJson rawFlow = new ObjectMapper().readValue(json, FlowJson.class);
			List<Scene> scenes = new ArrayList<Scene>();

			for (CellJson cell : rawFlow.cells)
			{
				switch (cell.type)
				{
					case "standard.Rectangle":
						Boolean isStart = cell.attrs.get("body").get("fill").equals("green");
						scenes.add(new Scene(cell.id, isStart, null));

						if (isStart)
						{
							currentScene = cell.id;
						}
						break;

					case "standard.Link":
						/// Loop through scenes and add correct target to the source scene
						for (int i = 0; i < scenes.size(); i++)
						{
							if (scenes.get(i).id.compareTo(cell.source.get("id")) == 0)
							{
								scenes.set(i, new Scene(scenes.get(i).id, scenes.get(i).start, cell.target.get("id")));
							}
						}
						break;
				}
			}

			this.scenes = scenes;
		}
		catch (StreamReadException e)
		{
			e.printStackTrace();
		}
		catch (DatabindException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}


		/// Then insert the data from the .scene files
		// into the scenes, like the text lines,
		// background id and background music id
		for (int i = 0; i < scenes.size(); i++)
		{
			try
			{
				// Load the .scene file, and convert it into a Map with
				// and unsafe type conversion, witch is ok in this case,
				// bacause the .scene file it's parsed from is very
				// controlled in content, and is predictable.
				InputStream is = Flow.class.getClassLoader().getResourceAsStream("scenes/" + scenes.get(i).id.toString() + ".scene");
				Map<String, Object> rawScene = new ObjectMapper().readValue(is, Map.class);

				// Convert just the "lines" sub list into a List of Maps
				List<Map<String, Object>> rawLines = new ObjectMapper().convertValue(rawScene.get("lines"), List.class);

				// Convert the list of raw Object lines into a sanitary List<Line>
				List<Line> lines = new ArrayList<Line>();
				for (Map<String, Object> line : rawLines)
				{
					lines.add(new Line((int) line.get("character"), (String) line.get("text")));
				}

				// Get the characters
				List<Object> rawCharacters = new ObjectMapper().convertValue(rawScene.get("characters"), List.class);

				// Convert the list of raw Object characters into a sanitary int[]
				int[] characters = new int[rawCharacters.size()];
				for (int k = 0; k < rawCharacters.size(); k++)
				{
					characters[k] = (int) rawCharacters.get(k);
				}
				

				/// Insert the data and replace the scenes
				Scene s = scenes.get(i);
				s.SetBackground((int) rawScene.get("background"));
				s.SetBackgroundMusic((int) rawScene.get("backgroundMusic"));
				s.SetLines(lines);
				s.SetCharacters(characters);
				scenes.set(i, s);
			}
			catch (StreamReadException e)
			{
				e.printStackTrace();
			}
			catch (DatabindException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	/// Getters
	/**
	 * Returns the loaded and
	 * parsed scenes
	 * 
	 * @return a list of scenes
	 */
	public List<Scene> GetScenes()
	{
		return scenes;
	}

	public Scene GetCurrentScene()
	{
		for (Scene scene : scenes)
		{
			if (scene.id.equals(currentScene))
			{
				return scene;
			}
		}
		
		return null;
	}

	private int GetCurrentSceneIndex()
	{
		for (int i = 0; i < scenes.size(); i++)
		{
			if (scenes.get(i).id.equals(currentScene))
			{
				return i;
			}
		}

		return -1;
	}

	private int GetSceneIndexFromUUID(UUID id)
	{
		for (int i = 0; i < scenes.size(); i++)
		{
			if (scenes.get(i).id.equals(id))
			{
				return i;
			}
		}

		return -1;
	}

	private UUID GetSceneIdFromIndex(int index)
	{
		return scenes.get(index).id;
	}

	public boolean HasNextLine()
	{
		Scene cs = GetCurrentScene();
		return (cs.lines.size() - 1) > cs.currentLine;
	}

	public void NextLine()
	{
		scenes.get(GetCurrentSceneIndex()).NextLine();
	}

	public boolean NextScene()
	{
		if (GetCurrentScene().target != null)
		{
			currentScene = GetSceneIdFromIndex(GetSceneIndexFromUUID(GetCurrentScene().target));
			return true;
		}

		return false;
	}

	@Override
	public String toString()
	{
		String ret = "[\n";

		for (Scene scene : scenes)
		{
			String sc = "\t{\n\t\t";
			sc += String.format("id: %s, start: %s, target: %s, bg: %d, bgMusic: %d,\n\t\t",
								scene.id, scene.start, scene.target, scene.background, scene.backgroundMusic);
			sc += String.format("characters: %s,\n\t\t", Arrays.toString(scene.characters));
			sc += "lines: \n\t\t[\n";

			for (Line l : scene.lines)
			{
				sc += "\t\t\t{\n";
				sc += "\t\t\t\tcharacter: " + l.character + ",\n";
				sc += "\t\t\t\ttext: \"" + l.text + "\"\n";
				sc += "\t\t\t},\n";
			}
			sc = sc.substring(0, sc.length() - 2);
			
			sc += "\n\t\t]\n";
			sc += "\t},\n";

			ret += sc;
		}
		ret = ret.substring(0, ret.length() - 2);

		return ret + "\n\n]";
	}
}