package io.nuvel.engine.math;

/**
 * Class to handle 2d vector math
 */
public class Vector2
{
	/// Public variables
	public int x;
	public int y;

	/**
	 * Constructor with two ints
	 * 
	 * @param x
	 * @param y
	 */
	public Vector2(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	/**
	 * Constructor from another vector
	 * 
	 * @param v the vector to copy from
	 */
	public Vector2(Vector2 v)
	{
		this.x = v.x;
		this.y = v.y;
	}
}