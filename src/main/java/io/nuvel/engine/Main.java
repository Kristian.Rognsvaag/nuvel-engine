package io.nuvel.engine;

import io.nuvel.engine.core.Controller;
import io.nuvel.engine.core.Loader;
import io.nuvel.engine.core.View;
import io.nuvel.engine.core.Window;
import io.nuvel.engine.math.Vector2;

public class Main
{
	public static void main(String[] args)
	{
		/// Setup variables
		// Initialize the loader
		Loader loader = Loader.Instance();
		// System.out.println(loader.flow);

		// Window variables
		final String WINDOW_TITLE = loader.GetSettings().get("Global", "Title");
		final Vector2 WINDOW_SIZE = new Vector2(1280, 720);
		final String WINDOW_IMAGE_ICON = "assets/images/logo.png";

		
		/// Create the window
		// Initialize view and controller
		View view = new View();
		new Controller(view);

		// Create and initialize the JFrame
		Window frame = new Window(WINDOW_TITLE, WINDOW_SIZE, view);


		/// Sets the icon of the window
		// P.S - Must be set after the setContentPane function
		frame.setWindowIcon(WINDOW_IMAGE_ICON);
	}
}