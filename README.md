# Nuvel Engine
The Nuvel Engine is the Java engine that actually compiles into the game executable. It is not intended to be compiled manually, as a server system will automatically place all the game file in the "lt" directory, as well as run the compilation.  

The repo for the web based editor is: [https://git.krirogn.dev/nuvel/editor](https://git.krirogn.dev/nuvel/editor), and
the repo for the backend PHP server is: [https://git.krirogn.dev/nuvel/api](https://git.krirogn.dev/nuvel/api). If there
are any question on how the system works, I would be more than happy to answer :)

The editor will be avilable on [nuvel.io](nuvel.io) in the near future as well.

## Videos
Demonstration:  
<video controls="true" allowfullscreen="true" style="width:400px;">
    <source src="https://backups-kfr.s3.fr-par.scw.cloud/demonstration2.mp4" type="video/mp4">
</video>
<br/>
Explanation:  
<video controls="true" allowfullscreen="true" style="width:400px;">
    <source src="https://backups-kfr.s3.fr-par.scw.cloud/explanation.mp4" type="video/mp4">
</video>

## VS Code
If you use VS Code, it is strongly encouraged to install the "fabiospampinato.vscode-highlight" extension, to get the custom support for comment highlighting. This will as an example make the #region code much easier to read. 

## Compiling
To compile the project run:
```shell
mvn clean compile assembly:single
```
in the terminal. This will create a .jar univeral executible file that
can be run with:
```shell
java -jar target/engine*.jar
```
in a terminal, or double click the .jar if your operating system allows
for it. 

## W.I.P
- [ ] Fix scene.target to array of targets

## ToDo
- [ ] Data
	- [x] Load flow chart
		- [x] Scene data
			- [x] Lines data
	- [x] Load assets
		- [x] Load background
		- [x] Load character sprite
			- [ ] Support multiple character "mood" sprites
		- [ ] Load SFX
		- [ ] Load music
		- [ ] Load fonts
		- [x] Load GUI elements (sprites)
- [x] Input
	- [ ] Add input system for keyboard and mouse
		- [x] Mouse
	- [ ] Support GUI buttons
- [ ] Graphics
	- [x] Implement JComponent drawing routine
	- [ ] Use fonts
	- [ ] Menu
		- [ ] Settings
		- [ ] Play
		- [ ] Load
			- [ ] Show saves and configurations
	- [x] Backgrounds
	- [ ] Transitions
	- [x] Characters
		- [x] Displayed
		- [ ] Positioned accordingly
		- [ ] Animated
- [ ] Audio
	- [ ] Music
	- [ ] SFX
- [ ] Compilation
	- [x] Create a Make file for compiling
	- [ ] Executable files are placed in the "dist" directory
- [ ] Platform Support
	- [ ] Compilation to universal .jar
	- [ ] Compilation to portable Windows .exe
	- [ ] Compilation to MacOS .app
		- [ ] With .dmg installer

## Game File Specs
The Nuvel Engine supports game files in the ".lt" format. The uncompressed folder should have the following file structure:
```
lt
│     settings.ini
│     menu.ini
│     flow.json
│
┣───📂scenes
│     │     UUID.scene
│     │     0584c861-4b3d-49a2-93a9-2aeef15b916f.scene
│     │     ...
│   
┣───📂characters
│     │     $.character
│     │     1.character
│     │     ...
│
└───📂assets
      │
      ┣───📂audio
      │     │     sfx.ogg
      │     │     click.ogg
      │     │     ...
      │
      ┣───📂backgrounds
      │     │     $.png
      │     │     1.png
      │     │     ...
      │
      ┣───📂characters
      │     │     $.png
      │     │     1.png
      │     │     ...
      │
      ┣───📂fonts
      │     │     font.ttf
      │     │     VerilySerifMono.ttf
      │     │     ...
      │
      ┣───📂images
      │     │     continue.png
      │     │     logo.png
      │     │     namebox.png
      │     │     textbox.png
      │     │     ...
      │
      └───📂music
            │     $.ogg
            │     1.ogg
            │     ...
```
All images must be in the .png format, all fonts must be in .ttf format and all audio must be in .ogg vorbis format. The .character files are just int the .ini format, and the .scene files are just JSON.  